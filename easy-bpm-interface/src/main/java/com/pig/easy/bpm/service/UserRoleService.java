package com.pig.easy.bpm.service;

import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.UserRoleDetailQueryDTO;
import com.pig.easy.bpm.dto.request.UserRoleReqDTO;
import com.pig.easy.bpm.dto.response.UserRoleDTO;
import com.pig.easy.bpm.dto.response.UserRoleDetailDTO;
import com.pig.easy.bpm.utils.Result;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pig
 * @since 2020-06-14
 */
public interface UserRoleService {

    Result<List<UserRoleDTO>> getUserRoleListByUserId(Long userId);

    Result<List<UserRoleDTO>> getUserRoleListByUserIdAndTenantId(Long userId,String tenantId);

    Result<List<UserRoleDTO>> getUserRoleListByRoleId(Long roleId);

    Result<List<UserRoleDTO>> getUserRoleListByRoleIdAndTenantId(Long roleId,String tenantId);

    Result<PageInfo> getListByCondiction(UserRoleReqDTO userRoleReqDTO);

    Result<UserRoleDTO> getUserRoleById(Long id);

    Result<Integer> insertUserRole(UserRoleReqDTO userRoleReqDTO);

    Result<Integer> updateUserRoleById(UserRoleReqDTO userRoleReqDTO);

    Result<List<UserRoleDetailDTO>> getUserRoleDetailByRoleId(Long roleId);

    Result<List<UserRoleDetailDTO>> getUserRoleDetailByUserId(Long userId);

    Result<List<UserRoleDetailDTO>> getUserRoleDetailByRoleIdAndUserId(Long roleId,Long userId);

    Result<List<UserRoleDetailDTO>> getUserRoleDetailByCondition(UserRoleDetailQueryDTO userRoleDetailQueryDTO);
}
