package com.pig.easy.bpm.service;

import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.DictItemQueryDTO;
import com.pig.easy.bpm.dto.request.DictItemSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.DictItemDTO;
import com.pig.easy.bpm.utils.Result;

import java.util.List;

/**
 * <p>
 * 字典详细表 服务类
 * </p>
 *
 * @author pig
 * @since 2020-05-28
 */
public interface DictItemService {

    Result<PageInfo<DictItemDTO>> getListByCondition(DictItemQueryDTO dictItemQueryDTO);

    Result<List<DictItemDTO>> getList(DictItemQueryDTO dictItemQueryDTO);

    Result<List<DictItemDTO>> getListByDictCode(String dictCode);

    Result<Integer> insertOrUpdate(DictItemSaveOrUpdateDTO dictItemSaveOrUpdateDTO);

}
