package com.pig.easy.bpm.service;

import com.pig.easy.bpm.dto.request.NotifyMessageDTO;
import com.pig.easy.bpm.utils.Result;

/**
 * todo:
 *
 * @author : zhoulin.zhu
 * @date : 2020/12/31 16:12
 */
public interface NotifyService {

    Result<Boolean> sendNotify(NotifyMessageDTO notifyMessageDTO);
}
