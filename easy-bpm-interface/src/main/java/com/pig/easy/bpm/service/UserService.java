package com.pig.easy.bpm.service;


import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.AddUserDTO;
import com.pig.easy.bpm.dto.request.UserQueryDTO;
import com.pig.easy.bpm.dto.request.UserUpdateDTO;
import com.pig.easy.bpm.dto.response.TreeDTO;
import com.pig.easy.bpm.dto.response.UserInfoDTO;
import com.pig.easy.bpm.utils.Result;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pig
 * @since 2020-05-14
 */
public interface UserService {

    /**
     * 功能描述: 根据员工编号生成 token ，如果 公司定制，可以重写该方法
     * 
     * 
     * @param userId 员工编号
     * @return : com.pig.easy.bpm.utils.Result<java.lang.String>
     * @author : pig
     * @date : 2020/5/15 11:40
     */
    Result<String> generateToken(Long userId);

    /**
     * 功能描述:  验证token 是否有效
     *
     * @param userId
     * @param token
     * @return : com.pig.easy.bpm.utils.Result<java.lang.Boolean>
     * @author : pig
     * @date : 2020/5/15 11:45
     */
    Result<UserInfoDTO> verifyToken(Long userId,String token);

    /**
     * 功能描述: 从token获取员工编号
     *
     *
     * @param token
     * @return : com.pig.easy.bpm.utils.Result<java.lang.Integer>
     * @author : pig
     * @date : 2020/5/15 11:45
     */
    Result<Long> getUserIdByToken(String token);

    /**
     * 功能描述:  根据员工编号 邮箱 用户名 登录
     *
     *
     * @param userName
     * @param password
     * @return : com.pig.easy.bpm.utils.Result<com.pig.easy.bpm.dto.response.UserInfoDTO>
     * @author : pig
     * @date : 2020/5/19 10:48
     */
    Result<UserInfoDTO> login(String userName,String password);

    /**
     * 功能描述:  根据员工编号 邮箱 用户名 查询用户
     *
     *
     * @param userName
     * @return : com.pig.easy.bpm.utils.Result<com.pig.easy.bpm.dto.response.UserInfoDTO>
     * @author : pig
     * @date : 2020/5/19 10:48
     */
    Result<UserInfoDTO> getUserInfo(String userName);

    Result<UserInfoDTO> getUserInfoById(Long userId);

    Result<UserInfoDTO> addUser(AddUserDTO addUserDTO);

    Result<Boolean> logout(Long userId, String accessToken);

    Result<PageInfo<UserInfoDTO>> getListByCondition(UserQueryDTO userQueryDTO);

    Result<List<UserInfoDTO>> getList(UserQueryDTO userQueryDTO);

    Result<List<TreeDTO>> getOrganUserTree(String tenantId, Long parentCompantId);

    Result<Integer> updateUser(UserUpdateDTO userUpdateDTO);
}
