package com.pig.easy.bpm.dto.response;

import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/24 10:48
 */
@Data
@ToString
public class RoleGroupDTO extends BaseResponseDTO {

    private static final long serialVersionUID = 2701520953647794002L;

    private Long roleGroupId;

    /**
     * 角色组编码
     */
    private String roleGroupCode;

    /**
     * 角色组名称
     */
    private String roleGroupName;

    /**
     * 角色组简称
     */
    private String roleGroupAbbr;

    /**
     * 角色组等级
     */
    private Integer roleGroupLevel;

    /**
     * 角色组类别
     */
    private Integer roleGroupType;

    /**
     * 所属条线
     */
    private String businessLine;

    /**
     * 租户编号
     */
    private String tenantId;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 状态 1 有效 0 失效
     */
    private Integer validState;

    /**
     * 操作人工号
     */
    private Long operatorId;

    /**
     * 操作人姓名
     */
    private String operatorName;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
}
