package com.pig.easy.bpm.dto.request;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/14 15:50
 */
@Data
@ToString
public class BaseRequestDTO implements Serializable {

    private static final long serialVersionUID = -4157414406762155070L;

    protected String system;

    protected String accessToken;

    protected Long  currentUserId;

    protected String currentUserName;

    protected String currentRealName;

    protected String tenantId;

    protected String paltform;

}
