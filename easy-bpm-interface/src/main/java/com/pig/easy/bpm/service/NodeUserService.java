package com.pig.easy.bpm.service;

import com.pig.easy.bpm.dto.request.NodeUserQueryDTO;
import com.pig.easy.bpm.dto.request.NodeUserSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.NodeUserDTO;
import com.pig.easy.bpm.utils.Result;

import java.util.List;

/**
 * <p>
 * 节点人员表 服务类
 * </p>
 *
 * @author pig
 * @since 2020-07-04
 */
public interface NodeUserService{

    Result<List<NodeUserDTO>> getListByCondition(NodeUserQueryDTO nodeUserQueryDTO);

    Result<List<NodeUserDTO>> getListByApplyId(Long applyId);

    Result<Integer> batchSave(List<NodeUserSaveOrUpdateDTO> nodeUserList);

    Result<Integer> updateNodeUserDefinitionIdByProcInscId(String procInscId,String definitionId);

    Result<Integer> insert(NodeUserSaveOrUpdateDTO nodeUserSaveOrUpdateDTO);
}
