package com.pig.easy.bpm.vo.request;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/26 18:45
 */
@Data
@ToString
public class DictQueryVO implements Serializable {

    private static final long serialVersionUID = 6185771081484846575L;


    private String dictCode;

    private String dictName;

    @NotEmpty
    @NotNull
    private String tenantId;

    private String remark;

    private Integer validState;

    private int pageIndex;

    private int pageSize;
}
