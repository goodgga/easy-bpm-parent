package com.pig.easy.bpm.annotations;

/**
 * todo:  控制是否需要记录 controller 请求与返回结果
 *
 * @author : pig
 * @since  : 2020/5/15 10:42
 */
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Log {

    boolean value() default true;

}
