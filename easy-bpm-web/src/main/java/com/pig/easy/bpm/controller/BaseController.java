package com.pig.easy.bpm.controller;

import com.pig.easy.bpm.context.GlobalUserInfoContext;
import com.pig.easy.bpm.dto.response.UserInfoDTO;
import com.pig.easy.bpm.utils.BeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/15 18:20
 */
@Slf4j
public class BaseController {

    protected UserInfoDTO currentUserInfo() {
        return GlobalUserInfoContext.getLoginInfo();
    }

    /**
     * 转换成 DTO
     *
     * @param source
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> T switchToDTO(Object source, Class<T> clazz) {
        T result = BeanUtils.objectToBean(source, clazz);
        BeanUtils.setProperty(result, "currentUserId", currentUserInfo() == null ? null : currentUserInfo().getUserId());
        BeanUtils.setProperty(result, "currentUserName", currentUserInfo() == null ? null : currentUserInfo().getUserName());
        BeanUtils.setProperty(result, "currentRealName", currentUserInfo() == null ? null : currentUserInfo().getRealName());
        BeanUtils.setProperty(result, "accessToken", currentUserInfo() == null ? null : currentUserInfo().getAccessToken());

        BeanUtils.setProperty(result, "tenantId", currentUserInfo() == null ? null : currentUserInfo().getTenantId());
        BeanUtils.setProperty(result, "system", currentUserInfo() == null ? null : currentUserInfo().getSystem());
        BeanUtils.setProperty(result, "paltform", currentUserInfo() == null ? null : currentUserInfo().getPaltform());

        return result;
    }

    protected String getRemoteIP(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-nginx-real-ip");
        if (StringUtils.isNotBlank(ipAddress)) {
            return ipAddress;
        }

        ipAddress = request.getHeader("X-Real-IP");
        if (StringUtils.isNotBlank(ipAddress)) {
            return ipAddress;
        }

        ipAddress = request.getHeader("X-Forwarded-For");
        if (StringUtils.isNotBlank(ipAddress)) {
            return ipAddress;
        }

        ipAddress = request.getHeader("X-Remote-Addr");
        if (StringUtils.isNotBlank(ipAddress)) {
            return ipAddress;
        }

        ipAddress = request.getHeader("HTTP_X_FORWARDED_FOR");
        if (StringUtils.isNotBlank(ipAddress)) {
            return ipAddress;
        }

        ipAddress = request.getHeader("Proxy-Client-IP");
        if (StringUtils.isNotBlank(ipAddress)) {
            return ipAddress;
        }

        ipAddress = request.getHeader("WL-Proxy-Client-IP");
        if (StringUtils.isNotBlank(ipAddress)) {
            return ipAddress;
        }

        ipAddress = request.getHeader("HTTP_X_REAL_IP");
        if (StringUtils.isNotBlank(ipAddress)) {
            return ipAddress;
        }

        if (StringUtils.isBlank(ipAddress)) {
            ipAddress = request.getRemoteAddr();
        }

        return ipAddress;
    }
}
