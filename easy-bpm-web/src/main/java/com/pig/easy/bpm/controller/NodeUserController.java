package com.pig.easy.bpm.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.pig.easy.bpm.controller.BaseController;

/**
 * <p>
 * 节点人员表 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-07-04
 */
@RestController
@RequestMapping("/nodeUserDO")
public class NodeUserController extends BaseController {

}

