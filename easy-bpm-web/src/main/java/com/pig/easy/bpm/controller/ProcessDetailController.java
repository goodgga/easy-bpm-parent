package com.pig.easy.bpm.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.dto.request.ProcessDetailReqDTO;
import com.pig.easy.bpm.dto.request.ProcessPublishDTO;
import com.pig.easy.bpm.dto.response.ProcessDetailDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.ProcessDetailService;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.CommonUtils;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import com.pig.easy.bpm.vo.request.ProcessDetailVO;
import com.pig.easy.bpm.vo.request.ProcessPublishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-06-06
 */
@Api(tags = "流程详细信息", value = "流程详细信息")
@RestController
@RequestMapping("/processDetail")
public class ProcessDetailController extends BaseController {

    @Reference
    ProcessDetailService processDetailService;

    @ApiOperation(value = "查询流程详细列表", notes = "查询流程详细列表", produces = "application/json")
    @PostMapping("/getList")
    public JsonResult getList(@ApiParam(name = "传入流程详细信息", value = "传入json格式", required = true) @Valid @RequestBody ProcessDetailVO processDetailVO) {

        BestBpmAsset.isAssetEmpty(processDetailVO);
        BestBpmAsset.isAssetEmpty(processDetailVO.getTenantId());
        ProcessDetailReqDTO processDetailReqDTO = switchToDTO(processDetailVO, ProcessDetailReqDTO.class);

        Result<PageInfo> result = processDetailService.getListByCondiction(processDetailReqDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "新增或修改流程详细信息", notes = "新增或修改流程详细信息", produces = "application/json")
    @PostMapping("/insertOrUpdate")
    public JsonResult insertOrUpdate(@Valid @RequestBody ProcessDetailVO processDetailVO) {

        BestBpmAsset.isAssetEmpty(processDetailVO);
        BestBpmAsset.isAssetEmpty(processDetailVO.getProcessId());
        BestBpmAsset.isAssetEmpty(processDetailVO.getTenantId());

        ProcessDetailReqDTO processDetailReqDTO = switchToDTO(processDetailVO, ProcessDetailReqDTO.class);
        processDetailReqDTO.setOperatorId(currentUserInfo().getUserId());
        processDetailReqDTO.setOperatorName(currentUserInfo().getRealName());
        Result<Integer> result = null;
        if (CommonUtils.evalLong(processDetailReqDTO.getProcessDetailId()) < 0) {
            result = processDetailService.insertProcessDetail(processDetailReqDTO);
        } else {
            result = processDetailService.updateProcessDetail(processDetailReqDTO);
        }
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "删除流程详细信息", notes = "删除流程详细信息", produces = "application/json")
    @PostMapping("/delete")
    public JsonResult delete(@Valid @RequestBody ProcessDetailVO processDetailVO) {

        BestBpmAsset.isAssetEmpty(processDetailVO);
        BestBpmAsset.isAssetEmpty(processDetailVO.getTenantId());
        BestBpmAsset.isAssetEmpty(processDetailVO.getProcessId());
        BestBpmAsset.isAssetEmpty(processDetailVO.getProcessDetailId());

        ProcessDetailReqDTO processDetailReqDTO = switchToDTO(processDetailVO, ProcessDetailReqDTO.class);
        processDetailReqDTO.setValidState(BpmConstant.INVALID_STATE);
        processDetailReqDTO.setOperatorId(currentUserInfo().getUserId());
        processDetailReqDTO.setOperatorName(currentUserInfo().getRealName());
        Result<Integer> result = processDetailService.updateProcessDetail(processDetailReqDTO);

        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "发布流程", notes = "发布流程", produces = "application/json")
    @PostMapping("/publish")
    public JsonResult publish(@RequestBody ProcessPublishVO processPublishVO) {

        BestBpmAsset.isAssetEmpty(processPublishVO);
        BestBpmAsset.isAssetEmpty(processPublishVO.getTenantId());
        BestBpmAsset.isAssetEmpty(processPublishVO.getProcessKey());
        BestBpmAsset.isAssetEmpty(processPublishVO.getProcessDetailId());
        BestBpmAsset.isAssetEmpty(processPublishVO.getProcessXml());

        ProcessPublishDTO processPublishDTO = switchToDTO(processPublishVO, ProcessPublishDTO.class);
        processPublishDTO.setOperatorId(currentUserInfo().getUserId());
        processPublishDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Boolean> result = processDetailService.processPublish(processPublishDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());

    }


    @ApiOperation(value = "修改默认版本", notes = "修改默认版本", produces = "application/json")
    @PostMapping("/updateDefaultVersion/{processId}/{processDetailId}")
    public JsonResult updateDefaultVersion(
            @ApiParam(required = true, name = "流程编号", value = "processId", example = "1") @PathVariable("processId") Long processId,
            @ApiParam(required = true, name = "流程详细编号", value = "processDetailId", example = "1") @PathVariable("processDetailId") Long processDetailId
    ) {
        Result<Boolean> result = processDetailService.updateDefaultVersion(processId, processDetailId, currentUserInfo().getUserId(), currentUserInfo().getRealName());
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());

    }

    @ApiOperation(value = "获取流程详细信息", notes = "获取流程详细信息", produces = "application/json")
    @PostMapping("/getProcessDetailById/{processDetailId}")
    public JsonResult getProcessDetailById(
            @ApiParam(required = true, name = "流程详细编号", value = "processDetailId", example = "1") @PathVariable("processDetailId") Long processDetailId
    ) {
        Result<ProcessDetailDTO> result = processDetailService.getProcessDetailById(processDetailId);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

}

