package com.pig.easy.bpm.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import com.alibaba.dubbo.config.annotation.Reference;
import com.pig.easy.bpm.vo.request.*;
import com.pig.easy.bpm.dto.request.*;
import com.pig.easy.bpm.dto.response.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.apache.commons.lang3.StringUtils;
import com.pig.easy.bpm.service.TenantService;
import java.util.List;
import javax.validation.Valid;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.dto.request.ProcessReqDTO;
import com.pig.easy.bpm.dto.response.DynamicFormDataDTO;
import com.pig.easy.bpm.dto.response.ProcessDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.ProcessService;
import com.pig.easy.bpm.utils.BestBpmAsset;


import org.springframework.web.bind.annotation.RestController;
import com.pig.easy.bpm.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * <p>
 * 租户表 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-09-02
 */
@RestController
@Api(tags = "租户表管理", value = "租户表管理")
@RequestMapping("/tenant")
public class TenantController extends BaseController {

        @Reference
        TenantService service;

        @ApiOperation(value = "查询租户表列表", notes = "查询租户表列表", produces = "application/json")
        @PostMapping("/getList")
        public JsonResult getList(@Valid @RequestBody TenantQueryVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        TenantQueryDTO queryDTO = switchToDTO(param, TenantQueryDTO.class);

        Result<PageInfo<TenantDTO>> result = service.getListByCondition(queryDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

        @ApiOperation(value = "新增租户表", notes = "新增租户表", produces = "application/json")
        @PostMapping("/insert")
        public JsonResult insertTenant(@Valid @RequestBody TenantSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
       TenantSaveOrUpdateDTO saveDTO = switchToDTO(param, TenantSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId());
        saveDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer> result = service.insertTenant(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

        @ApiOperation(value = "修改租户表", notes = "修改租户表", produces = "application/json")
        @PostMapping("/update")
        public JsonResult updateTenant(@Valid @RequestBody TenantSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
         TenantSaveOrUpdateDTO saveDTO = switchToDTO(param, TenantSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId());
        saveDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer> result = service.updateTenant(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

        @ApiOperation(value = "删除租户表", notes = "删除租户表", produces = "application/json")
        @PostMapping("/delete")
        public JsonResult delete(@Valid @RequestBody TenantSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        TenantSaveOrUpdateDTO saveDTO = switchToDTO(param, TenantSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId());
        saveDTO.setOperatorName(currentUserInfo().getRealName());
        saveDTO.setValidState(BpmConstant.INVALID_STATE);

        Result<Integer> result = service.deleteTenant(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

}

