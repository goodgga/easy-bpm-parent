package com.pig.easy.bpm.service.impl;


import com.pig.easy.bpm.dto.request.NotifyMessageDTO;
import com.pig.easy.bpm.service.NotifyService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class NotifyServiceImplTest {

    @Autowired
    NotifyService notifyService;

    @Test
    public void sendNotify() {

        NotifyMessageDTO notifyMessageDTO = new NotifyMessageDTO();
        notifyMessageDTO.setContent("test sendEamail ");
        List<String> sendToUserEmailList = new ArrayList<>();
        sendToUserEmailList.add("zhoulin.zhu@zhenai.com");
        notifyMessageDTO.setSendToUserEmailList(sendToUserEmailList);
        notifyMessageDTO.setEventCode("PROCESS_START");
        notifyMessageDTO.setMessageTypeCode("HTML");
        notifyMessageDTO.setPaltForm("PC");
        notifyService.sendNotify(notifyMessageDTO);
    }


}