package com.pig.easy.bpm.service.impl;

import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.NotifyMessageDTO;
import com.pig.easy.bpm.dto.request.UserQueryDTO;
import com.pig.easy.bpm.dto.response.ApplyDTO;
import com.pig.easy.bpm.dto.response.MessageContentDTO;
import com.pig.easy.bpm.dto.response.UserInfoDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.*;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.CommonUtils;
import com.pig.easy.bpm.utils.Result;
import com.pig.easy.bpm.utils.SendEmailUtils;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboService;
import org.flowable.engine.HistoryService;
import org.flowable.variable.api.history.HistoricVariableInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactory;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/12/31 16:20
 */
@Slf4j
@DubboService
public class NotifyServiceImpl implements NotifyService {

    @Value("${email.smtpHost}")
    private String smtpHost;

    @Value("${email.smtpPort}")
    private Integer smtpPort;

    @Value("${email.smtpUsername}")
    private String smtpUser;

    @Value("${email.smtpPassword}")
    private String smtpPass;

    @Autowired
    MessageContentService messageContentService;
    @Autowired
    UserService userService;
    @Resource
    FreeMarkerConfigurationFactory freeMarkerConfigurationFactory;
    @Resource
    HistoryService historyService;
    @Autowired
    ApplyService applyService;
    @Autowired
    FormDataService formDataService;



    @Override
    public Result<Boolean> sendNotify(NotifyMessageDTO notifyMessageDTO) {

        BestBpmAsset.isAssetEmpty(notifyMessageDTO);
        BestBpmAsset.isAssetEmpty(notifyMessageDTO.getEventCode());
        BestBpmAsset.isAssetEmpty(notifyMessageDTO.getPaltForm());

        List<String> sendToUserEmailList = notifyMessageDTO.getSendToUserEmailList() != null ? notifyMessageDTO.getSendToUserEmailList() : new CopyOnWriteArrayList<>();

        if (notifyMessageDTO.getSendToUserIdList() != null && notifyMessageDTO.getSendToUserIdList().size() > 0) {
            List<Long> userIdList = new CopyOnWriteArrayList<>();
            UserQueryDTO userQueryDTO = new UserQueryDTO();
            userQueryDTO.setUserIdList(userIdList);
            userQueryDTO.setPageSize(10000);
            Result<PageInfo<UserInfoDTO>> result = userService.getListByCondition(userQueryDTO);
            if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
                return Result.responseError(result.getEntityError());
            }
            result.getData().getList().stream().forEach(userInfoDTO -> {
                sendToUserEmailList.add(userInfoDTO.getEmail());
            });
        }

        String content = notifyMessageDTO.getContent();
        String subject = StringUtils.isEmpty(notifyMessageDTO.getSubject()) ? "EasyBpm":notifyMessageDTO.getSubject();
        if (StringUtils.isEmpty(content)){
            Result<MessageContentDTO> result = messageContentService.getMessageContent(notifyMessageDTO.getProcessId(), notifyMessageDTO.getTenantId(), notifyMessageDTO.getEventCode(), notifyMessageDTO.getMessageTypeCode(), notifyMessageDTO.getPaltForm());
            if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
                return Result.responseError(result.getEntityError());
            }
             content = result.getData().getMessageContent();
        }

        Map<String, Object> businessData = Collections.synchronizedMap(new HashMap<>());

        if(CommonUtils.evalInt(notifyMessageDTO.getApplyId())>0){

            Result<Map<String, Object>> result = formDataService.getFormDataByApplyId(notifyMessageDTO.getApplyId());
            if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
                return Result.responseError(result.getEntityError());
            }
            businessData.putAll(result.getData());

            Result<ApplyDTO> result2 = applyService.getApplyByApplyId(notifyMessageDTO.getApplyId());
            if (result2.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
                return Result.responseError(result2.getEntityError());
            }
            ApplyDTO applyDTO = result2.getData();
            List<HistoricVariableInstance> list = historyService.createHistoricVariableInstanceQuery().processInstanceId(applyDTO.getProcInstId()).list();
            for (HistoricVariableInstance historicVariableInstance : list) {
                businessData.put(historicVariableInstance.getVariableName(), historicVariableInstance.getValue());
            }
        }

        if (notifyMessageDTO.getParamMap() != null && notifyMessageDTO.getParamMap().size() > 0) {
            businessData.putAll(notifyMessageDTO.getParamMap());
        }

        content = formatSendContent(content, businessData);
        try {
            log.info("sendEmail info params: {}", notifyMessageDTO);
            SendEmailUtils.getInstance().sendEmail(smtpHost, smtpPort, smtpUser, smtpPass, sendToUserEmailList.toArray(new String[sendToUserEmailList.size()]), subject, content);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.responseError(EntityError.SYSTEM_ERROR);
        }

        return Result.responseSuccess(Boolean.TRUE);
    }

    private String formatSendContent(String content,Map<String,Object> businessData ){

        String formatContent = "";
        try {
            Configuration configuration = freeMarkerConfigurationFactory.createConfiguration();
            StringTemplateLoader stringLoader = new StringTemplateLoader();
            stringLoader.putTemplate("easyBpmTemplate", content);
            configuration.setTemplateLoader(stringLoader);
            freemarker.template.Template temp = null;
            temp = configuration.getTemplate("easyBpmTemplate", "utf-8");
            StringWriter stringWriter = new StringWriter(2048);
            temp.process(businessData, stringWriter);
            formatContent = stringWriter.toString();
            stringWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }

        return formatContent;
    }

}
