package com.pig.easy.bpm.service.impl;

import com.pig.easy.bpm.entity.RoleGroupToRoleDO;
import com.pig.easy.bpm.mapper.RoleGroupToRoleMapper;
import com.pig.easy.bpm.service.RoleGroupToRoleService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author pig
 * @since 2020-06-14
 */
@Service
public class RoleGroupToRoleServiceImpl extends BeseServiceImpl<RoleGroupToRoleMapper, RoleGroupToRoleDO> implements RoleGroupToRoleService {

    @Autowired
    RoleGroupToRoleMapper roleGroupToRoleMapper;
}
