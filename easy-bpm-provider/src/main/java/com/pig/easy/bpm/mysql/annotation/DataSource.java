package com.pig.easy.bpm.mysql.annotation;

import java.lang.annotation.*;

@Target( {ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataSource {

    /**
     *  use  DataSource  name
     *
     */
    String name();
}


