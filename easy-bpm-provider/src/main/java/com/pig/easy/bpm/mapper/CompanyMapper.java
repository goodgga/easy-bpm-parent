package com.pig.easy.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig.easy.bpm.dto.response.CompanyDTO;
import com.pig.easy.bpm.entity.CompanyDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 公司表 Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-05-20
 */
@Mapper
public interface CompanyMapper extends BaseMapper<CompanyDO> {

    List<CompanyDTO> getListByCondition(CompanyDO companyDO);

    Integer updateByCompanyCode(CompanyDO companyDO);
}
