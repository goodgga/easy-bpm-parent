package ${package.Controller};


import org.springframework.web.bind.annotation.RequestMapping;
import com.alibaba.dubbo.config.annotation.Reference;
import ${cfg.voRequest}.*;
import ${cfg.dtoRequest}.*;
import ${cfg.dtoResponse}.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.apache.commons.lang3.StringUtils;
import ${package.Service}.${table.serviceName};
import java.util.List;
import javax.validation.Valid;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.dto.request.ProcessReqDTO;
import com.pig.easy.bpm.dto.response.DynamicFormDataDTO;
import com.pig.easy.bpm.dto.response.ProcessDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.ProcessService;
import com.pig.easy.bpm.utils.BestBpmAsset;


#if(${restControllerStyle})
import org.springframework.web.bind.annotation.RestController;
#else
import org.springframework.stereotype.Controller;
#end
#if(${superControllerClassPackage})
import ${superControllerClassPackage};
#end
#if(${swagger2})
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

#end

/**
 * <p>
 * $!{table.comment} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
#if(${restControllerStyle})
@RestController
#else
@Controller
#end
#if(${swagger2})
@Api(tags = "$!{table.comment}管理", value = "$!{table.comment}管理")
#end
@RequestMapping("#if(${package.ModuleName})/${package.ModuleName}#end/#if(${controllerMappingHyphenStyle})${controllerMappingHyphen}#else${table.entityPath}#end")
#if(${kotlin})
class ${table.controllerName}#if(${superControllerClass}) : ${superControllerClass}()#end

#else
#if(${superControllerClass})
public class ${table.controllerName} extends ${superControllerClass} {
#else
public class ${table.controllerName} {
#end

        @Reference
        ${table.serviceName} service;

        @ApiOperation(value = "查询$!{table.comment}列表", notes = "查询$!{table.comment}列表", produces = "application/json")
        @PostMapping("/getList")
        public JsonResult getList(@Valid @RequestBody ${entity}QueryVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        ${entity}QueryDTO queryDTO = switchToDTO(param, ${entity}QueryDTO.class);

        Result<PageInfo<${entity}DTO>> result = service.getListByCondition(queryDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

        @ApiOperation(value = "新增$!{table.comment}", notes = "新增$!{table.comment}", produces = "application/json")
        @PostMapping("/insert")
        public JsonResult insert${entity}(@Valid @RequestBody ${entity}SaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
       ${entity}SaveOrUpdateDTO saveDTO = switchToDTO(param, ${entity}SaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId());
        saveDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer> result = service.insert${entity}(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

        @ApiOperation(value = "修改$!{table.comment}", notes = "修改$!{table.comment}", produces = "application/json")
        @PostMapping("/update")
        public JsonResult update${entity}(@Valid @RequestBody ${entity}SaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
         ${entity}SaveOrUpdateDTO saveDTO = switchToDTO(param, ${entity}SaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId());
        saveDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer> result = service.update${entity}(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

        @ApiOperation(value = "删除$!{table.comment}", notes = "删除$!{table.comment}", produces = "application/json")
        @PostMapping("/delete")
        public JsonResult delete(@Valid @RequestBody ${entity}SaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        ${entity}SaveOrUpdateDTO saveDTO = switchToDTO(param, ${entity}SaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId());
        saveDTO.setOperatorName(currentUserInfo().getRealName());
        saveDTO.setValidState(BpmConstant.INVALID_STATE);

        Result<Integer> result = service.delete${entity}(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

}

#end